// Select the section with an id of container without using querySelector.

const containerEl = document.getElementById('container')

// console.log(containerEl);

// Select the section with an id of container using querySelector.

const queryContinerEl = document.querySelector('#container')

// console.log(queryContinerEl);

//  Select all of the list items with a class of "second".

const secondClassEl = document.getElementsByClassName('second')
// console.log(secondClassEl);

// Select a list item with a class of third, but only the list item inside of the ol tag.

const thirdLiEl = document.querySelector('ol .third')
// console.log(thirdLiEl);

// Give the section with an id of container the text "Hello!".

// containerEl.innerHTML += 'Text'

// containerEl.prepend('Text');

//  Add the class main to the div with a class of footer.

const footerEl = document.querySelector('div.footer')
footerEl.classList.add('main')

// console.log(footerEl);

// Remove the class main on the div with a class of footer.
setTimeout(()=>{
    footerEl.classList.remove('main')
    // console.log(footerEl);
},1000)



//  Create a new li element.

const newListEl = document.createElement('li')

// Give the li the text "four".
newListEl.textContent = 'four'

// Append the li to the ul element.
const unorderListEl = document.querySelector('ul')
unorderListEl.appendChild(newListEl)

// Loop over all of the list inside the ol tag and give them a background color of "green".

const orderListEl = document.querySelectorAll('ol li')
// console.log(orderListEl);

// orderListEl.forEach((list)=> list.style.backgroundColor = 'green')


// for (let list of orderListEl){
//     list.style.backgroundColor = 'green'
// }

// Remove the div with a class of footer. 
// console.log(document);
const divFooterEl = document.querySelector('div.footer')

// const bodyEl = document.querySelector('body')
// console.log(bodyEl);
// bodyEl.removeChild(divFooterEl);
// document.querySelector('body').removeChild(divFooterEl)

